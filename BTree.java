
public class BTree implements BTreeInterface {

	private BNode root;
	private final int t;

	/**
	 * Construct an empty tree.
	 */
	public BTree(int t) { //
		this.t = t;
		this.root = null;
	}

	// For testing purposes.
	public BTree(int t, BNode root) {
		this.t = t;
		this.root = root;
	}

	@Override
	public BNode getRoot() {
		return root;
	}

	@Override
	public int getT() {
		return t;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((root == null) ? 0 : root.hashCode());
		result = prime * result + t;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BTree other = (BTree) obj;
		if (root == null) {
			if (other.root != null)
				return false;
		} else if (!root.equals(other.root))
			return false;
		if (t != other.t)
			return false;
		return true;
	}
	

	@Override
	public Block search(int key) {
		// TODO Auto-generated method stub
		return root.search(key);
	}

	@Override
	public void insert(Block b) {
		// TODO Auto-generated method stub
		if( b == null) return;
		
		//if root is null creat new root
		//with one block b
		if( root == null ) root = new BNode(t,b);
		
		//else if root is full
		else if(root.isFull()){
			
			//make new root and split old root
			root = new BNode(t,root);
			root.splitChild(0);
			
			//and perform insert
			root.insertNonFull(b);
		}
		else root.insertNonFull(b);
		
	}

	@Override
	public void delete(int key) {
		// TODO Auto-generated method stub
		if(root == null) return;
		
		//if our tree if just the root then delete key
		else if(root.isLeaf()) root.deleteBlockFromLeaf(key);
		
		//else delete recursively
		else root.delete(key);
	}

	@Override
	public MerkleBNode createMBT() {
		// TODO Auto-generated method stub
		return root.createHashNode();
	}


}
