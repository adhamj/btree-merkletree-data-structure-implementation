import java.util.ArrayList;

//SUBMIT
public class BNode implements BNodeInterface {

	private final int t;
	private int numOfBlocks;
	private boolean isLeaf;
	private ArrayList<Block> blocksList;
	private ArrayList<BNode> childrenList;

	/**
	 * Constructor for creating a node with a single child.<br>
	 * Useful for creating a new root.
	 */
	public BNode(int t, BNode firstChild) {
		this(t, false, 0);
		this.childrenList.add(firstChild);
	}

	/**
	 * Constructor for creating a <b>leaf</b> node with a single block.
	 */
	public BNode(int t, Block firstBlock) {
		this(t, true, 1);
		this.blocksList.add(firstBlock);
	}

	public BNode(int t, boolean isLeaf, int numOfBlocks) {
		this.t = t;
		this.isLeaf = isLeaf;
		this.numOfBlocks = numOfBlocks;
		this.blocksList = new ArrayList<Block>();
		this.childrenList = new ArrayList<BNode>();
	}

	// For testing purposes.
	public BNode(int t, int numOfBlocks, boolean isLeaf,
			ArrayList<Block> blocksList, ArrayList<BNode> childrenList) {
		this.t = t;
		this.numOfBlocks = numOfBlocks;
		this.isLeaf = isLeaf;
		this.blocksList = blocksList;
		this.childrenList = childrenList;
	}

	@Override
	public int getT() {
		return t;
	}

	@Override
	public int getNumOfBlocks() {
		return numOfBlocks;
	}

	@Override
	public boolean isLeaf() {
		return isLeaf;
	}

	@Override
	public ArrayList<Block> getBlocksList() {
		return blocksList;
	}

	@Override
	public ArrayList<BNode> getChildrenList() {
		return childrenList;
	}

	@Override
	public boolean isFull() {
		return numOfBlocks == 2 * t - 1;
	}

	@Override
	public boolean isMinSize() {
		return numOfBlocks == t - 1;
	}
	
	@Override
	public boolean isEmpty() {
		return numOfBlocks == 0;
	}
	
	@Override
	public int getBlockKeyAt(int indx) {
		return blocksList.get(indx).getKey();
	}
	
	@Override
	public Block getBlockAt(int indx) {
		return blocksList.get(indx);
	}

	@Override
	public BNode getChildAt(int indx) {
		return childrenList.get(indx);
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((blocksList == null) ? 0 : blocksList.hashCode());
		result = prime * result
				+ ((childrenList == null) ? 0 : childrenList.hashCode());
		result = prime * result + (isLeaf ? 1231 : 1237);
		result = prime * result + numOfBlocks;
		result = prime * result + t;
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BNode other = (BNode) obj;
		if (blocksList == null) {
			if (other.blocksList != null)
				return false;
		} else if (!blocksList.equals(other.blocksList))
			return false;
		if (childrenList == null) {
			if (other.childrenList != null)
				return false;
		} else if (!childrenList.equals(other.childrenList))
			return false;
		if (isLeaf != other.isLeaf)
			return false;
		if (numOfBlocks != other.numOfBlocks)
			return false;
		if (t != other.t)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "BNode [t=" + t + ", numOfBlocks=" + numOfBlocks + ", isLeaf="
				+ isLeaf + ", blocksList=" + blocksList + ", childrenList="
				+ childrenList + "]";
	}

	
	@Override
	public Block search(int key) {
		
		//find index of where key should be
		int i = 0;
		while(i < this.getNumOfBlocks() && key > this.getBlockKeyAt(i))
			i = i+1;
		
		//if key is found return
		if(i < this.getNumOfBlocks() && key == this.getBlockKeyAt(i) )
			return this.getBlockAt(i);
		
		//else if this is a leaf key does not exist
		else if(this.isLeaf) return null;
		
		//else go down to child and search recursively
		else return this.getChildAt(i).search(key);
	}

	@Override
	public void insertNonFull(Block d) {
		
		//find index of where key should be
		int i = this.getNumOfBlocks() -1;
		while(i >= 0 && d.getKey() < this.getBlockKeyAt(i))
			i--;
		i++;
		
		//if we have a leaf
		if(this.isLeaf()){
			//add and update block count
			this.blocksList.add(i,d);
			this.numOfBlocks++;
		}
		
		//if this is not a leaf
		else{
			
			//and if child is full
			if(this.getChildAt(i).isFull()){
				
				//then split child
				this.splitChild(i);
				
				//check if we gotta update index after splitting
				if(this.getBlockKeyAt(i) < d.getKey()) i++;
			}
			
			//insert recursively
			this.getChildAt(i).insertNonFull(d);
		}
	}

	@Override
	public void delete(int key) {
		
		//if we are in a leaf then delete
		if(this.isLeaf()) this.deleteBlockFromLeaf(key);
		
		//if not a leaf
		else {
			
			//find index if where key should be
			int indx=0;
			while(this.numOfBlocks-1>=indx && key>this.getBlockKeyAt(indx))
				indx = indx+1;
			
			//if found delete
			if(this.numOfBlocks-1>=indx && key==this.getBlockKeyAt(indx)){
				this.deleteBlockNonLeaf(indx,key);
			}
			
			//if not found then its in children
			else{
				
				//we check if merge or shift is needed for child
				boolean merge = this.shiftOrMergeChildIfNeeded(indx);
				
				//if we executed a merge then we check 
				//again and recursively delete 
				if(merge) this.delete(key); 
		
				//else if we did a shift we go
				//into child and delete recursively 
				else this.getChildAt(indx).delete(key);
						
				
			}
		}
	}

	@Override
	public MerkleBNode createHashNode() {
		
		//empty MBNode
		MerkleBNode MBNode;
		
		//if this is a laf then return MerkleBnode 
		if( this.isLeaf())
			MBNode = new MerkleBNode(HashUtils.sha1Hash(this.getData()));
		
		//if not a leaf
		else {
			
			
			//empty ArrayList to hold data
			ArrayList<byte[]> data = new ArrayList<byte[]>();
			ArrayList<MerkleBNode> children = new ArrayList<MerkleBNode>();
			for( int i=0 ; i <= this.getNumOfBlocks() ; i++){
			
				//we add MBNode children to Children ArrayList
				children.add(this.getChildAt(i).createHashNode());
				
				//we add hashValue to data ArrayList
				data.add(children.get(i).getHashValue());
				if(i!=this.getNumOfBlocks())	
					data.add(this.getBlockAt(i).getData());
				
			}
			//create MerkleBNode
			MBNode = new MerkleBNode(HashUtils.sha1Hash(data),false,children);
		}
		return MBNode;
	}
	
	
	
	
	
	
	
	// //////////////////////////////////////////////////////
	// ///////////////Helper functions bellow////////////////
	// //////////////////////////////////////////////////////
	
	
	
	// ============== Merging & Splitting ===================
	
	
	//splits child at index
	public void splitChild(int index){
		
		//we initialize left and right nodes
		BNode y = this.getChildAt(index);
		BNode z = new BNode(t,y.isLeaf,t-1);
		
		//we fill the right node with the right 
		//half of blocks in original node
		z.blocksList = new ArrayList<Block>(y.blocksList.subList(t,2*t-1));
		
		//if child is not a leaf , add children 
		//to the split nodes
		if(!(y.isLeaf())){
			z.childrenList = new ArrayList<BNode>(y.childrenList.subList(t, 2*t));
			y.childrenList = new ArrayList<BNode>(y.childrenList.subList(0,t));
		}
		//set number of blocks , t-1 because we split a full node
		y.numOfBlocks = t-1;
		
		//add median block to parent node
		//and update block count
		this.blocksList.add(index, y.getBlockAt(t-1));
		this.numOfBlocks++;
		
		//add right child to children list
		this.childrenList.add(index+1, z);
		
		//fill the left node with the left 
		//half of the blocks
		y.blocksList = new ArrayList<Block>(y.blocksList.subList(0,t-1));
		
			
	}
	
	//return true if child had at least t blocks
	private boolean childHasNonMinimalLeftSibling(int childIndx){

		//if index is legal
		if(childIndx > 0 && this.getChildAt(childIndx-1)!=null)
			//return true if node has at least t blocks
			return this.getChildAt(childIndx-1).getNumOfBlocks() > t-1;
		//else return false
		return false;
	}

	//return true if child had at least t blocks
	private boolean childHasNonMinimalRightSibling(int childIndx){
		
		//if index is legal
		if(childIndx < this.getNumOfBlocks() && this.getChildAt(childIndx+1)!=null)
			//return true if node has at least t blocks
			return this.getChildAt(childIndx+1).getNumOfBlocks() > t-1;
		//else return false
		return false;
	}

	//returns a boolean value indicating the following :
	//	true = a merge was executed.
	//	false = either a shift or nothing.
	private boolean shiftOrMergeChildIfNeeded(int childIndx){
		
		//if child has minimal block count
		if(this.getChildAt(childIndx).getNumOfBlocks() < this.getT()){
			//and if child has non minimal left sibling
			if(this.childHasNonMinimalLeftSibling(childIndx))
				//execute a shift from left
				this.shiftFromLeftSibling(childIndx);
			
			//if child doesnt have non minimal left sibling
			else {
				//check if it has non minimal right sibling
				if(this.childHasNonMinimalRightSibling(childIndx))
					//if so , execute a shift from the right
					this.shiftFromRightSibling(childIndx);
				
				//otherwise both left and right siblings
				//are minimal, so we execute a merge
				else return this.mergeChildWithSibling(childIndx);
				
			}
		}
		
		return false;
	}
	
	
	//perform a shift from left sibling
	private void shiftFromLeftSibling(int ci) {
		
		//point at left sibling
		BNode leftBro = this.getChildAt(ci-1);
		
		//point at block to the lift of Bi
		Block dadBlock = this.getBlockAt(ci-1);
		
		//get the last (largest key) block in left sibling
		Block leftBlock = leftBro.getBlockAt(leftBro.getNumOfBlocks()-1);
		
		//add dadBlock as the first block in child 
		this.getChildAt(ci).blocksList.add(0, dadBlock);
		
		//replace dadBlock with maximum value less than dadBlock
		this.blocksList.set(ci-1, leftBlock);
		
		//remove last block in sibling
		leftBro.blocksList.remove(leftBro.getNumOfBlocks()-1);
		
		//if sibling is not a leaf
		if(!leftBro.isLeaf()){
			//add last child of sibling as first child of child
			this.getChildAt(ci).childrenList.add(0, leftBro.getChildAt(leftBro.getNumOfBlocks()));
			//remove it from sibling
			leftBro.childrenList.remove(leftBro.getNumOfBlocks());
		}
		
		//update block count in sibling & child
		leftBro.numOfBlocks--;
		this.getChildAt(ci).numOfBlocks++;
	}

	//perform a shift from right sibling
	private void shiftFromRightSibling(int ci) {
		//point at right sibling
		BNode rightBro = this.getChildAt(ci+1);
		
		//point at block to the right of Bi
		Block dadBlock = this.getBlockAt(ci);
		
		//get the first (smallest key) block in right sibling
		Block leftBlock = rightBro.getBlockAt(0);
		
		//add dadBlock to child 
		this.getChildAt(ci).blocksList.add(dadBlock);
		
		//replace dadBlock with rightBlock
		this.blocksList.set(ci, leftBlock);
		
		//remove last block in sibling
		rightBro.blocksList.remove(leftBlock);
		
		//if sibling is not a leaf
		if(!rightBro.isLeaf()){
			//add first child of sibling as last child of child
			this.getChildAt(ci).childrenList.add(rightBro.getChildAt(0));
			//remove it from sibling
			rightBro.childrenList.remove(rightBro.getChildAt(0));
		}
				
		//update block count in sibling & child
		rightBro.numOfBlocks--;
		this.getChildAt(ci).numOfBlocks++;
	}

	//merge child with sibling
	private boolean mergeChildWithSibling(int childIndx) {
		
		//if left sibling exists 
		if(childIndx > 0 && this.getChildAt(childIndx-1)!=null){
			//merge with left sibling
			this.mergeWithLeftSibling(childIndx);
			return true;
		}
		
		else
			//if right sibling exists
			if(childIndx < this.getNumOfBlocks() && this.getChildAt(childIndx+1) != null){
				//merge with right sibling
				this.mergeWithRightSibling(childIndx);
				return true;
			}
			//else return false indicating merge was not executed 
			else
				return false;
	}	
	
	//merge child with left sibling
	private void mergeWithLeftSibling(int childIndx) {

		//point at the right child
		BNode leftChild = this.getChildAt(childIndx-1);
		
		//point at blockList and childrenList
		ArrayList<Block> leftChildBlocks = leftChild.blocksList;
		ArrayList<BNode> leftChildChildren = leftChild.childrenList;
		
		//remove the left child since its will be merged
		//with the right sibling
		this.childrenList.remove(childIndx-1);
		
		//pointer to the block where we merged left and right children at
		Block parentBlock = this.getBlockAt(childIndx-1);
		
		//delete the block from parent node & update block count
		this.blocksList.remove(childIndx-1);
		this.numOfBlocks--;
			
			
		//add parentBlock & left child blocks and children
		//to right child thus merging the two.
		this.getChildAt(childIndx-1).blocksList.add(0, parentBlock);
		this.getChildAt(childIndx-1).numOfBlocks++;
		for(int i=leftChildBlocks.size()-1;i>=0;i=i-1){
			this.getChildAt(childIndx-1).blocksList.add(0, leftChildBlocks.get(i));
			this.getChildAt(childIndx-1).numOfBlocks++;
		}
		//if its not a leaf add children
		if(!leftChild.isLeaf())
			for(int i=leftChildChildren.size()-1;i>=0;i=i-1)
				this.getChildAt(childIndx-1).childrenList.add(0, leftChildChildren.get(i));
		
		//if this is root and root is empty
		if(this.getNumOfBlocks()==0){
			
			BNode child = this.getChildAt(childIndx-1);
			this.childrenList.remove(childIndx-1);
			this.blocksList = child.getBlocksList();
			
			if(!child.isLeaf())
				this.childrenList = child.getChildrenList();
			else
				this.childrenList = new ArrayList<BNode>();
			
			this.numOfBlocks = this.blocksList.size();
			this.isLeaf = child.isLeaf;
		}
	}
	
	//merge child with right sibling

	
	//merge child with right sibling
	private void mergeWithRightSibling(int childIndx){
		
		//point at the right child
		BNode rightChild = this.getChildAt(childIndx+1);
		
		//point at blockList and childrenList
		ArrayList<Block> rightChildBlocks = rightChild.blocksList;
		ArrayList<BNode> rightChildChildren = rightChild.childrenList;
		
		//remove the right child since its will be merged
		//with thr left sibling
		this.childrenList.remove(rightChild);
		
		//pointer to the block where we merged left and right children at
		Block parentBlock = this.getBlockAt(childIndx);
		
		//delete the block from parent node & update block count
		this.blocksList.remove(parentBlock);
		this.numOfBlocks--;
		
		
		//add parentBlock & right child blocks and children to left child
		//to left child thus merging the two.
		this.getChildAt(childIndx).blocksList.add(parentBlock);
		this.getChildAt(childIndx).numOfBlocks++;
		for(int i=0;i<rightChildBlocks.size();i=i+1){
			this.getChildAt(childIndx).blocksList.add(rightChildBlocks.get(i));
			this.getChildAt(childIndx).numOfBlocks++;
		}
		if(!rightChild.isLeaf())
			for(int i=0;i<rightChildChildren.size();i=i+1)
				this.getChildAt(childIndx).childrenList.add(rightChildChildren.get(i));
		

		//if this is root and root is empty
		if(this.getNumOfBlocks()==0){
			BNode child = this.getChildAt(childIndx);
			this.childrenList = new ArrayList<BNode>();
			
			//add blocks
			this.blocksList.addAll(child.blocksList);
			
			//if the child is not a leaf add children
			if(!child.isLeaf())
				this.childrenList.addAll(child.childrenList);
			//update block count and is leaf
			this.numOfBlocks = child.getNumOfBlocks(); 
			this.isLeaf =  child.isLeaf;
		}
		
	}
	
	
	// ===================== Deleting =========================
	
	//delete a block that is in a leaf
	public void deleteBlockFromLeaf(int key){
		
		//find blocks position
		int i=0;
		while( this.numOfBlocks-1 >= i && key > this.getBlockKeyAt(i)) i++;
		
		//if block exists
		if( this.numOfBlocks-1 >= i && key == this.getBlockKeyAt(i) ){
			
				//remove block from list & update block count
				this.blocksList.remove(i);
				this.numOfBlocks--;
		}
	}
	
	
	//delete a block that is in internal node
	private void deleteBlockNonLeaf(int indx,int key){
		
		//pointers at left and right children of wanted block
		BNode leftChild = this.getChildAt(indx);
		BNode rightChild = this.getChildAt(indx+1);
		
		//if the left child is non minimal
		if(leftChild.getNumOfBlocks() >= this.getT()){
			
			//find the predecessor & replace
			//parent block with predecessor
			this.blocksList.set(indx, leftChild.getPredecessor(this.getBlockAt(indx)));
			
			//recursively delete block from child
			this.getChildAt(indx).delete(key);
		}
		
		//else if the right child is non minimal
		else if( rightChild.getNumOfBlocks() >= this.getT() ){

				//find the successor & replace
				//parent block with successor
				this.blocksList.set(indx,rightChild.getSuccessor(this.getBlockAt(indx)));
				
				//recursively  delete block from child
				this.getChildAt(indx+1).delete(key);
		}
		else{
			
			//if both children are minimal
			//execute a merge
			this.mergeWithRightSibling(indx);
			//delete key from merged child
			this.delete(key);
		}
	}
	
	
	
	
	// ===================== Hashing ===========================
	
	
	public ArrayList<byte[]> getData() {
		
		//how much data we have got
		int size = this.getBlocksList().size();
		
		//new ArrayList to return
		ArrayList<byte[]> data = new ArrayList<byte[]>();
		
		//add data to ArrayList
		for(int i=0 ; size > i ; i++)
			data.add(this.getBlockAt(i).getData());
		
		//return data
		return data;
		
	}
	
	
	
	
	// ===================== Misc. =============================
	
	
	//return predecessor of a given block
	private Block getPredecessor(Block block){
		
		//get index of the last child
		int i = this.getNumOfBlocks();
		
		//empty block
		Block predecessor = null;
		
		//if this node is a leaf
		if(this.isLeaf()){
			
			//set predecessor as max key
			predecessor = this.getMaxKeyBlock();
			//change block
			this.blocksList.set(i-1,block);
		}
		//else search recursively 
		else predecessor = this.getChildAt(i).getPredecessor(block);
		
		//return predecessor
		return predecessor;
	}
	
	
	// A function to get Successor of key
	private Block getSuccessor(Block block){
		
		//empty block
		Block successor = null;
		
		//if this node is a leaf
		if(this.isLeaf()){
			
			//get the minimum key ( successor )
			successor = this.getMinKeyBlock();
			
			//change block
			this.blocksList.set(0, block);
		}
		
		//if this node is not a leaf then 
		//search recursively for seccessor
		else successor = this.getChildAt(0).getSuccessor(block);
		
		//return successor
		return successor;
	}
	
	
	//return minimum key in node
	private Block getMinKeyBlock(){
		return this.blocksList.get(0);
	}
	
	//return maximum key in node
	private Block getMaxKeyBlock(){
		return this.blocksList.get(this.getNumOfBlocks()-1);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	

}
